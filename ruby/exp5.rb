#!/usr/bin/env ruby

puts "Enter three number to find the greatest of three: "

a = []
for _ in 1..3
  i = gets.chomp()
  a.push(i)
end

c = a[2]
b = a[1]
a = a[0]

if a >= b and a >= c
  puts "#{a} is greatest"
elsif b >= c and b >= a
  puts "#{b} is greatest"
else 
  puts "#{c} is greatest"
end 
