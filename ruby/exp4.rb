#!/usr/bin/env ruby

puts "Enter filename: "
file_name = gets.chomp()

len = file_name.length()
puts len

for i in (0..len)
  if file_name[i] == "."
    i += 1
    print "file extension is "
    while i < len
      print file_name[i]
      i += 1
    end
  end
end  
