#!/usr/bin/env ruby

student_marks = {   
    "sl" => 89,   
    "cd" => 99,   
    "daa" => 100,   
    "ml" =>  200,
  }   

total_marks = student_marks["sl"] + student_marks["ml"]+ student_marks["daa"]+ student_marks["cd"]

print "Total marks: #{total_marks}"
