#!/usr/bin/env ruby

print "Odd numbers from 10 to 1 are ,"
i = 10
while i >= 1
  if i%2 != 0
    print " #{i}"
  end
  i -= 1
end
