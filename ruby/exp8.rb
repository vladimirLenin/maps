#!/usr/bin/env ruby


puts "Enter two numbers: "

a = []
for _ in 1..2
  i = gets.chomp()
  a.push(i)
end

temp1 = a[1].to_i
temp2 = a[0].to_i

def my_func(a,b)
  if (a < 0 and b > 100) or (a > 100 and b < 0)
    print true
  end
end

print my_func(temp1,temp2)
