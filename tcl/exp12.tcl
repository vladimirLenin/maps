#!/usr/bin/tclsh

# puts "Enter a number: "
set num 10 
# v[gets stdin]

# for loop execution
set result  1
for { set i 1}  {$i <= $num} {incr i} {
	set result [expr $result * $i]
}
puts "Result is: "
puts $result

