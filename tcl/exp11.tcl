#!/usr/bin/tclsh

puts "Enter a number: "
set num [gets stdin]

# for loop execution
set factorial  1
for { set i 1}  {$i <= $num} {incr i} {
	set factorial [expr $factorial * $i]
}
puts "Factorial is: "
puts $factorial
