#include <stdio.h>

int n,k;
int data[100],poly[100],res[100];

void crc(int *rem);

int main(){
  int i,rem[100];
  printf("Enter no.of payload bits: ");
  scanf("%d",&n);

  printf("Enter  payload bits: ");
  for(i=0;i<n;i++)
    scanf("%d",&data[i]);

  printf("Enter no.of polynomial bits: ");
  scanf("%d",&k);

  printf("Enter polynomial bits: ");
  for(i=0;i<k;i++)
    scanf("%d",&poly[i]);

  crc(rem);

  for(i=0;i<k-1;i++)
    printf("%d",rem[i]);

}

void crc(int *rem){
  int i, *op1, *op2, all_zero[100], p, temp[100];
    for(i=0; i<k; i++)
        temp[i] = data[i];
    op1 = temp;
    for(i=0; i<k; i++)
        all_zero[i] = 0;
    for(p=k; p<=n+k-1; p++)
    {
        if(op1[0] == 0)
            op2 = all_zero;
        else
            op2 = poly;
        for(i=1; i<k; i++)
            if(op1[i] == op2[i])
                res[i-1] = 0;
            else
                res[i-1] = 1;
        res[i-1] = data[p];
        op1=res;
    }
    for(i=0; i<k-1; i++)
        rem[i] = res[i];
}
