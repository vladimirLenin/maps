#include <stdio.h>

#define INFINITY 9999;

int minM(int d[],int adj[]){
  int i=1;
  int min;
  int k;
  min = d[adj[i]];
  while(adj[i]){
    if(min >= d[adj[i]]){
      min = d[adj[i]];
      k = adj[i];
    }
    i++;
  }
  return k;
}

void labeL(int pointer,int adj[],int d[30],int graph[30][30]){
  int i = 1;
    while(adj[i]){
      if (d[adj[i]] > d[pointer]+graph[pointer][adj[i]] ) {
        d[adj[i]] = d[pointer]+graph[pointer][adj[i]];
      }
      i++;
    }
}

int isOutgoing(int pointer,int graph[30][30],int n){
  int i;
  for(i=1;i<=n;i++)
    if (graph[pointer][i]!=0)
      return 1;
  return 0;
}

void adjacent(int pointer,int graph[30][30],int n,int adj[]){
  int i,j=1;
  for(i=1;i<=n;i++)
    if(graph[pointer][i]!=0){
      adj[j]=i;
      j++;
    }
}

void traversal(int start,int graph[][30],int n,int d[],int path[]){
  int i=2;
  int pointer=start;
  d[pointer] = 0;
  path[1] = start;

  while(isOutgoing(pointer,graph,n)){
    int adj[30]= {0};
    adjacent(pointer,graph,n,adj);
    labeL(pointer,adj,d,graph);
    pointer = minM(d,adj);
    path[i] = pointer;
    i++;
  }
}

int main(){
  int graph[30][30];
  int i,j,n,start,path[30]={0};
  int d[30];
  printf("Enter no.of nodes: ");
  scanf("%d",&n);
  printf("Enter cost matrix:\n ");
  for(i=1;i<=n;i++){
    d[i] = INFINITY;
  }
  for(i=1;i<=n;i++)
    for(j=1;j<=n;j++)
      scanf("%d",&graph[i][j]);

  printf("Enter start node: ");
  scanf("%d",&start);
  printf("Cost Matrix: \n");
  for(i=1;i<=n;i++){
    for(j=1;j<=n;j++)
      printf("%d ",graph[i][j]);
    printf("\n");
    }
  traversal(start,graph,n,d,path);
  i = 1;
  printf("Path : ");
  while(path[i]){
    printf("  %d ",path[i]);
    if(i!= n-1)
      printf(" -> ");
    i++;
    }
}
