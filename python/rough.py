import math
import sys
from functools import partial

sys.setrecursionlimit(10**6)
# def test(emacs="vamshi"):

    # print(emacs)
    # 
myDict = {"name": "John", "country": "Norway"}
mySeparator = "TEST"

x = mySeparator.join(myDict)

# print(x)

class Symbol(str):
    pass


def h(cin,ten,stay,shop,tot):
    return (-(cin/tot)*math.log2(cin/tot)-(ten/tot)*math.log2(ten/tot)-(stay/tot)*math.log2(stay/tot)-(shop/tot)*math.log2(shop/tot))
# def h(cin,ten,tot):
#     return (-(cin/tot)*math.log2(cin/tot)-(ten/tot)*math.log2(ten/tot)) #-(stay/tot)*math.log2(stay/tot)-(shop/tot)*math.log2(shop/tot))
# def h(cin,tot):
#     return (-(cin/tot)*math.log2(cin/tot)) #-(ten/tot)*math.log2(ten/tot)) 

def factorial(n):
    if (n==0): return 1
    else: return n*factorial(n-1)

print(factorial(5))

def isOdd(n):
    if (n%2==0): return False
    else: return True

def quickSort(xs):

    if xs == []:
        return []
    x = xs[0]
    smallerOrEqual = [a for a in xs if a <= x ]
    larger = [a for a in xs if a > x]
    # print("emax")
    # print(smallerOrEqual)
    # print(larger)
    return (quickSort(smallerOrEqual) + [x] + quickSort(larger))

def plus4(a):
    return a+4

def lessThan0(a,b):
    return a <= b

# n=int(input())
# l=[int(i) for i in input().split()]

# l = [4, 2, 3 ,4 ,4 ,9, 98, 98, 3, 3, 3 ,4, 2, 98, 1 ,98 ,98 ,1 ,1 ,4 ,98, 2, 98, 3 ,9 ,9, 3, 1, 4, 1, 98, 9 ,9, 2, 9 ,4 ,2, 2, 9, 98, 4, 98, 1, 3, 4, 9, 1, 98, 98, 4, 2, 3, 98, 98, 1, 99, 9, 98, 98, 3, 98, 98, 4, 98, 2, 98, 4, 2, 1, 1, 9, 2, 4]
l = [1 ,2 ,2 ,3 ,1 ,2]
maximum=0
for i in l:
    c=l.count(i)
    d=l.count(i-1)
    c=c+d
    if c > maximum:
        maximum=c
print(maximum)
