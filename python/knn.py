#!/usr/bin/env python3


import csv
import math

with open(r'DigiDB_movelist.csv') as csvfile:
    lines = csv.reader(csvfile)
    for row in lines:
        print(', '.join(row))



def euclideanDistance(x1,y1,x2,y2):
    return math.sqrt((x2-x1)**2 + (y2-y1)**2)
x = euclideanDistance(6,8,3,4)    
print(x)
