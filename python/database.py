#!/usr/bin/env python3
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="12345",
    database = "ml"
)

# print(mydb)

cursor = mydb.cursor()
query1 = "desc Persons"


cursor.execute(query1)

table = cursor.fetchall()

print('\n Table description: ')
for attr in table:
    print(attr)

query2 = "select * from Persons" 

cursor.execute(query2)

table = cursor.fetchall()


print('\n Table data: ')
for row in table:
    print(row[0] ,end=" ")
    print(row[1] ,end=" ")
    print(row[2] ,end=" ")
    print(row[3] ,end=" ")
    print(row[4] ,end="\n")
    
