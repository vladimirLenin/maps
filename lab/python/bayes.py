
def getPofFandA():
    return(int(input("Enter probability of that it is Friday and that a student is absent: ")))

def getPofF():
    return(int(input("Enter probability of that it is Friday: ")))

def bayesRule(pOfAandB,pOfA):
    return((pOfAandB/pOfA)*100.0)


if __name__ == '__main__':
    pOfAandB = getPofFandA()
    pOfA     = getPofF() 
    print("Probability that a student is absent given that today is Friday:" ,bayesRule(pOfAandB,pOfA))
    
