<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title>
	  My configuration setup
      </title>
  </head>
  <body style="background-color:black">
      <h1 style="color:lightgreen" > How I customized my PC </h1>
      <UL Type =“Value”>
	<LI style="color:white"> OS: Nixos </LI>
	<LI style="color:white"> DE: XMonad </LI>
	<LI style="color:white"> TE: eMACS </LI>
      </UL>
      <p>
	    <h2 style="color:lightpink">  Operating System: </h2> 
	    <h3 style="color:white">After travelling through all GNU/Linux based operating systems(like Ubuntu,Arch Linux..etc), I found NIXOS to be an operating system which fulfils my needs.</h3>
	    <img width="700" src="./images/nixos.png" alt= "Nixos logo" />
	    <a href="https://nixos.org"  target="_blank" style="color:white"><h3>Nixos official website!!</h1></a>
      </p>
  </body>
</html>
