(ns dawn.core
  (:gen-class))
   
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (+ 1 2))
  (println "Hello, World!")
  )
(defn mass []
  (println "mass!"))
(defn messenger
  ([]     (messenger "Hello world!"))
  ([msg]  (println msg)))
;; (defn mass []
  ;; (println "mass"))

;; (def severity :mild)
;;  (def error-message "OH GOD! IT'S A DISASTER! WE'RE ")
;; ;; (if (= severity :mild)
;;   ;; (def error-message (str error-message "MILDLY INCONVENIENCED!"))
;;   (def error-message (str error-message "DOOOOOOOMED!")))

(defn too-enthusiastic
   "Return a cheer that might be a bit too enthusiastic"
  [name]
  (str "OH. MY. GOD! " name " YOU ARE MOST DEFINITELY LIKE THE BEST "
       "MAN SLASH WOMAN EVER I LOVE YOU AND WE SHOULD RUN AWAY SOMEWHERE"))

(defn weird-arity
  ([]
   "Destiny dressed you this morning, my friend, and now Fear is
trying to pull off your pants. If you give up, if you give in,
you're gonna end up naked with Fear just standing there laughing
at your dangling unmentionables! - the Tick")
  ([number]
   (inc number)))

;; (defn announce-treasure-location
;;   [{lat :lat lng :lng}]
;;   (println (str "Treasure lat: " lat))
;;   (println (str "Treasure lng: " lng)))

(defn announce-treasure-location
  [{:keys [lat lng]}]
  (println (str "Treasure lat: " lat))
  (println (str "Treasure lng: " lng)))

;; (defn receive-treasure-location
;;   [{:keys [lat lng] :as treasure-location}]
;;   (println (str "Treasure lat: " lat))
;;   (println (str "Treasure lng: " lng))
;;   ;; One would assume that this would put in new coordinates for your ship
;;   (steer-ship! treasure-location))

(defn recursive-printer
([]
 (recursive-printer 0))
  ([iteration]
   (println iteration)
   (if (> iteration 3)
     (println "Goodbye!")
     (recursive-printer (inc iteration)))))
;; (recursive-printer)
(defn ass []
  (loop [iteration 0]
  (println (str "Iteration " iteration))
  (if (> iteration 3)
    (println "Goodbye!")
    (recur (inc iteration)))))


(def asym-hobbit-body-parts [{:name "head" :size 3}
                             {:name "left-eye" :size 1}
                             {:name "left-ear" :size 1}
                             {:name "mouth" :size 1}
                             {:name "nose" :size 1}
                             {:name "neck" :size 2}
                             {:name "left-shoulder" :size 3}
                             {:name "left-upper-arm" :size 3}
                             {:name "chest" :size 10}
                             {:name "back" :size 10}
                             {:name "left-forearm" :size 3}
                             {:name "abdomen" :size 6}
                             {:name "left-kidney" :size 1}
                             {:name "left-hand" :size 2}
                             {:name "left-knee" :size 2}
                             {:name "left-thigh" :size 4}
                             {:name "left-lower-leg" :size 3}
                             {:name "left-achilles" :size 1}
                             {:name "left-foot" :size 2}])

(defn matching-part
  [part]
  {:name (clojure.string/replace (:name part) #"^left-" "right-")
   :size (:size part)})

(defn symmetrize-body-parts
  "Expects a seq of maps that have a :name and :size"
   [asym-body-parts]
   (loop [remaining-asym-parts asym-body-parts
        final-body-parts []]
   (if (empty? remaining-asym-parts)
   final-body-parts
(let [[part & remaining] remaining-asym-parts]
   (recur remaining
        (into final-body-parts
      (set [part (matching-part part)])))))))
