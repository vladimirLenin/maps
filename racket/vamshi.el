;;; vamshi.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 vamshi krishna
;;
;; Author: vamshi krishna <https://github.com/vamshi>
;; Maintainer: vamshi krishna <vamshi5070k@gmail.com>
;; Created: April 09, 2021
;; Modified: April 09, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/vamshi/vamshi
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:



(provide 'vamshi)
;;; vamshi.el ends here
