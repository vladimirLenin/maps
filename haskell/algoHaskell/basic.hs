-- perms1
main = do
  putStr "hello"
  putStrLn "emacs"
  putStrLn "\nmass\n"
  putStrLn  "emacs is better than vscode "
  
-- perms1 [] = []
-- perms1 (x:xs) = [ zs | ys <- perms1 xs , zs <- inserts x ys ]

-- perms1 = foldr step [[]] where step x xss = concatMap (inserts x) xss
inserts :: a -> [a] -> [[a]]
inserts x [] = [[x]]
inserts x (y:ys) = (x:y:ys):map (y:) (inserts x ys)

labbe :: [Integer]
labbe = concatMap (\x -> [x+2]) [4,4]

-- step x  = concatMap (inserts x)

perms1 :: [a] -> [[a]]
perms1 = foldr (concatMap . inserts) [[]]

-- perms2 [] = [[]]
-- perms2 xs = [x]

picks :: [a] -> [(a,[a])]
picks [] = []
picks (x:xs) = (x,xs):[(y,x:ys)|(y,ys) <- picks xs]
-- map' :: (a -> b) ->  [a]  -> [b]
-- map' f  = foldr (\x -> f x  )  []
-- masx :: [(a,b)]
masx  = [(y,1:ys) | (y,ys) <- []]

perms2 :: [a] -> [[a]]
perms2 [] = [[]]
-- perms2 xs = [x : zs | (x, ys) <- picks xs, zs <- perms2 ys]
perms2 xs = concatMap subperms (picks xs)
            where subperms (x,ys) = map (x:) (perms2 ys)

concat' :: [[a]] -> [a]
concat' = foldr (++) []

collapse :: [[Int]] -> [Int]
collapse xss = help [] xss
help xs xss = if sum xs > 0 || null xss then xs
              else help (xs ++ head xss) (tail xss)

collapse1 xss = help1 (0, [ ]) (labelsum xss)
labelsum xss = zip (map sum xss) xss
help1 (s, xs) xss = if s > 0 || null xss then xs
                   else help1 (cat (s, xs) (head xss)) (tail xss)

cat (s, xs) (t, ys) = (s + t, xs ++ ys)

uncons' :: [a] -> Maybe (a,[a])
uncons' [] = Nothing
uncons' (x:xs) = Just (x , xs)

wrap :: a -> [a]
wrap x = [x]

unwrap :: [a] -> a
unwrap [x] = x

single :: [a] -> Bool
single (x:xs) = if null xs then True else False

reverse' :: [a] -> [a]
reverse'  =  foldl  (\acc x -> x:acc) []

map' :: (a -> b) -> [a] -> [b]
-- map' f xs = foldr (\x xs -> f x : xs) []
map' f = foldr ((:).f) []

filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldr (\x xs  -> if p x then x : xs else xs) []

-- emacs f e p = foldr f e . filter p

-- emacs' f e p  = filter p (foldr f e )

jabillikosam :: [Integer]
jabillikosam = [3..12]

pooja = [x+2 |  x <- [1,2,3]]
