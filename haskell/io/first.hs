main = do
  line <- getLine
  if null line
    then return ("ov[]er")
    else do
         putStrLn $ reverseWords line
         main

reverseWords :: String -> String
reverseWords = unwords . map reverse . words
