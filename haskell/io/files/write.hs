import System.IO

main = do
  withFile "new.txt" WriteMode (\handle -> do
                                   contents <- getLine
                                   hPutStr handle contents)
