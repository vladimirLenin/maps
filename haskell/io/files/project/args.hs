import System.IO
import Data.List
import System.Directory
import Control.Exception
import System.Environment

view ::  String -> IO ()
view nameOfFile =  do
                    contents <- readFile nameOfFile
                    let todoTasks = lines contents
                        numberedTasks = zipWith (\n line -> show n ++ " - " ++ line)
                                            [0..] todoTasks
                    putStr $ unlines numberedTasks

bump fileName numberString = do
                             contents <- readFile fileName
                             let todoTasks = lines contents
                             let number = read numberString
                                 temp = todoTasks !! number
                                 newTodoItems1 = [temp] ++ delete temp todoTasks
                                 newTodoItems2 = unlines newTodoItems1
                                 
                             bracketOnError (openTempFile "." "temp")
                                 (\(tempName, tempHandle) -> do
                                   hClose tempHandle
                                   removeFile tempName)
                                 (\(tempName, tempHandle) -> do
                                   hPutStr tempHandle newTodoItems2
                                   hClose tempHandle
                                   removeFile "todo.txt"
                                   renameFile tempName "todo.txt")

add nameOfFile todoItem  = appendFile nameOfFile (todoItem ++ "\n")  

remove fileName numberString =  do
                                contents <- readFile fileName
                                let todoTasks = lines contents
                                --     numberedTasks = zipWith (\n line -> show n ++ " - " ++ line)
                                --                     [0..] todoTasks
                                -- putStrLn "These are your TO-DO items:"
                                -- mapM_ putStrLn numberedTasks
                                let number = read numberString
                                    newTodoItems = unlines $ delete (todoTasks !! number) todoTasks
                                bracketOnError (openTempFile "." "temp")
                                 (\(tempName, tempHandle) -> do
                                   hClose tempHandle
                                   removeFile tempName)
                                 (\(tempName, tempHandle) -> do
                                   hPutStr tempHandle newTodoItems
                                   hClose tempHandle
                                   removeFile "todo.txt"
                                   renameFile tempName "todo.txt")

main = do
  args <- getArgs
  -- progName <- getProgName
  -- putStrLn "The arguments are:"
  -- mapM putStrLn args
  -- putStr "First argument: "
  let mode = args !! 0
  let nameOfFile = args !! 1
  -- | (mode == "view") =  putStrLn contents
  -- | otherwise = "You\'re a whore!"
  case mode of "view" ->   view   nameOfFile
               "add"  ->   add    nameOfFile (args !! 2)
               "remove" -> remove nameOfFile (args !! 2)
               "bump" -> bump nameOfFile (args !! 2)
               _ -> putStrLn "This command doesn\'t exist."
              -- "remove"
  -- -- if mode == "view"
  --   then
  --     putStrLn contents
  -- else
  --     putStr "lava"
  -- putStrLn "The program name is:"
  -- putStrLn progName
