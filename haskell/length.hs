
length' [] = 0
length' (x:xs) = 1 + length' xs

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (x:xs) = f x :(map' f xs)


emacs = "smdsd"

