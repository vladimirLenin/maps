one :: Integer
one = 1

string :: String
string  = "string"

false' :: Bool
false' = False

madam = maxBound :: Int

num = [1..10]
evenNum = [2,4..10]
oddNum = [3,5..11]
alphaList = ['a','b'..'z']
sumNum = sum num
prodNum = product num

two = elem 100 num

infibs = 0 : 1 : zipWith (+) infibs (tail infibs)

-- factorial 0 = 1
-- factorial n = n*factorial(n-1)

isOdd n
  | n `mod` 2 == 0 = False
  | otherwise = True

isEven n
  | isOdd n == True = False
  | otherwise = True

-- elem' [] _ = False
-- elem' (x:xs) k
--   | x == k = True
--   | otherwise = elem' xs k

-- average :: Int -> Int -> Int
sumBy2 a b =  (a+b)/2

average a b = floor $ sumBy2 a b

-- bsearch xs k =
--   let mid = average (head xs) (last xs)
  
-- max' xs = (\x -> ) foldr

vamshi :: String
vamshi = ['a'..'z']

triples = [(a,b,c) | a <- [1..10],b <- [1..10],c <- [1..10]]

rightTriangle = [(a,b,c) | c <- [1..10],a <- [1..c],b <- [1..a],a^2+b^2 == c^2,a+b+c == 24 ]

lucky :: Int -> String
lucky 7 = "seven"
lucky x = "not seven"

sayMe :: Int -> String
sayMe x = if x `elem` [1..5] then "In five" else "Not in five"

factorial 0 = 1
factorial n = n * factorial (n-1)

type Vector = (Double,Double)

addVectors :: Vector -> Vector -> Vector
addVectors a b = (fst a + fst b , snd a + snd b)

-- initials :: String -> String -> String
-- initials fname lname = [f] ++ ". " ++ [l] ++ "."
--     where (f:_) = fname
--           (l:_) = lname

initials :: String -> String -> String
initials (f:_) (l:_) = [f] ++ ". " ++ [l] ++ "."

describeList ls = case ls of []  -> "empty"
                             [x] -> "one"
                             _  -> "not emaunta"

maximum' :: (Ord a) => [a] -> a
-- maximum' [] = error "Empty list"
-- maximum' [x] = x
-- maximum' (x:xs) = (\x y -> if x > y then x else y) x (maximum' xs)
maximum' xs
        | (xs == []) = error "Empty list"
        | (length xs == 1) = head xs
        | otherwise =  (\x y -> if x > y then x else y) (head xs) (maximum' $ tail xs)

replicate' :: Int -> Int -> [Int]
replicate' n x
        | n <= 0 = []
        | otherwise =  x: replicate' (n-1) x
        
take' :: Int -> [a] -> [a]
take' n _
      | n <= 0 = []
take' _ []  = []
take' n (x:xs) = x : (take' (n-1) xs) 

reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

zip' :: [a] -> [a] -> [(a,a)]
zip'  [] _ = []
zip'  _ [] = []
zip' (x:xs) (y:ys) = (x,y) : zip' xs ys

elem' k [] = False
elem' k (x:xs) = if k == x then True else (elem' k xs)

quickSort :: (Ord a) =>  [a] -> [a]
quickSort [] = []
quickSort (x:xs) =
        let smallOrEqual = filter (<=x) xs--[a | a <- xs ,a <= x]
            larger = filter (>x) xs --[a | a <- xs , a > x]
        in quickSort smallOrEqual ++ [x] ++ quickSort larger

isUpperAlpha :: Char -> Bool
isUpperAlpha = (`elem` ['A'..'Z'])

                              --  MAP FILTER REDUCE  --

map' :: (a -> b) -> [a] -> [b]
map' _ [] = []
map' f (x:xs) = f x : map' f xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' _ [] = []
filter' p (x:xs) = if p x then x: filter' p xs else filter' p xs

null' :: [a] -> Bool
null' [] = True
null' _ = False

notNull a = not (null' a)

 -- head (filter p [100000,99999..])
largestDivisible :: [Integer]
largestDivisible =  (filter p [100000,99999..])
          where p x = x `mod` 3829 == 0

collatz :: Int -> [Int]
collatz 1 = [1]
collatz n
      | even n = n : collatz (n `div` 2)
      | otherwise = n : collatz (n*3 + 1)
      
numLongChains :: Int
numLongChains = length (filter isLong (map collatz [1..100]))
        where isLong xs = length xs > 15

listOfFuns = map (*) [0..]

flip' :: (a -> b -> c) -> b -> a -> c
flip' f = \x y ->  f y x

sum' :: (Num a) => [a] -> a
sum' = foldl (+) 0

pickingNum arr a= 
  map  ((\x y -> (  x == y || x-y == 1)) a) arr

pickingNumMap arr =
  map (pickingNum arr) arr

filterMap arr =
  length $ filter(\x -> x) arr

-- thala = [4, 2, 3 ,4 ,4 ,9, 98, 98, 3, 3, 3 ,4, 2, 98, 1 ,98 ,98 ,1 ,1 ,4 ,98, 2, 98, 3 ,9 ,9, 3, 1, 4, 1, 98, 9 ,9, 2, 9 ,4 ,2, 2, 9, 98, 4, 98, 1, 3, 4, 9, 1, 98, 98, 4, 2, 3, 98, 98, 1, 99, 9, 98, 98, 3, 98, 98, 4, 98, 2, 98, 4, 2, 1, 1, 9, 2, 4] 

thala = [1 ,2 ,2 ,3 ,1 ,2]



sieve (p:xs) = p : sieve [x | x <- xs , x `mod` p /= 0]
