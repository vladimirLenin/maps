
main = putStr "mass"

solveRPN :: String -> Double
solveRPN  = head . foldl foldingFunction [] . words



foldingFunction (x:y:ys) "*" = (y * x)  : ys
foldingFunction (x:y:ys) "+" = (y + x)  : ys
foldingFunction (x:y:ys) "-" = (y - x)  : ys
foldingFunction (x:y:ys) "/" = (y / x)  : ys
foldingFunction   xs   "sum" = sum xs : xs
foldingFunction (x:y:ys) "^" = (y ** x) : ys
foldingFunction xs numberString = read numberString : xs


