quickSort :: (Ord a) => [a] -> [a]
quickSort [] = []
quickSort (x:xs) =
  let smallerOrEqual = [a | a <- xs , a <= x]
      larger = [a | a <- xs , a > x]
  in quickSort smallerOrEqual ++ [x] ++ quickSort larger

applyTwice :: (a -> a) -> a -> a
applyTwice f x = f $ f x

myFoldr :: (t1 -> t2 -> t2) -> t2 -> [t1] -> t2

myFoldr f z []     = z
myFoldr f z (x:xs) = x `f` myFoldr f z xs

myFoldr' f z [] = z
myFoldr' f z (x:xs) = seq z $ x `f` myFoldr' f z xs

myFoldl :: (t1 -> t2 -> t1) -> t1 -> [t2] -> t1

myFoldl f z [] = z
myFoldl f z (x:xs) = myFoldl f (z `f` x) xs

myFoldl' f z [] = z
myFoldl' f z (x:xs) = seq (z `f` x) $ myFoldl f (z `f` x) xs

res = scanl1 (+)  [3,5,2,1]

sqrtSums :: Int
sqrtSums = length (takeWhile (<1000) (scanl1 (+) (map sqrt [1..]))) + 1
