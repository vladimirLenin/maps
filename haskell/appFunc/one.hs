main :: IO()
main = do
  line <- reverse <$> getLine
  putStrLn $ "You said " ++ line

mass :: Integer -> Integer
mass = (+2)

emacs :: String
emacs = "mass a"

eco :: Integer 
eco = 2

mas :: Integer
mas = 23

masss = fmap (replicate 3) (Just 4)
-- emacs :: String -> String
-- emacs = <$> (*3) 1
-- masssss = map (replicate 3) (Just 4)

aish :: (Functor f, Num b ) => f b -> f b
aish  = fmap  ((+2) . (+3))

kal = fmap (\x y z -> x + y / z) [3,4,5,6]

arr = fmap (*) [1..4]

shankar = fmap (\f -> f 4) arr
