{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_appFunc (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/vamshi/.cabal/bin"
libdir     = "/home/vamshi/.cabal/lib/x86_64-linux-ghc-8.10.2/appFunc-0.1.0.0-inplace-appFunc"
dynlibdir  = "/home/vamshi/.cabal/lib/x86_64-linux-ghc-8.10.2"
datadir    = "/home/vamshi/.cabal/share/x86_64-linux-ghc-8.10.2/appFunc-0.1.0.0"
libexecdir = "/home/vamshi/.cabal/libexec/x86_64-linux-ghc-8.10.2/appFunc-0.1.0.0"
sysconfdir = "/home/vamshi/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "appFunc_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "appFunc_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "appFunc_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "appFunc_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "appFunc_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "appFunc_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
