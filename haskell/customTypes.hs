import qualified Data.Map as Map

mass :: Bool
mass = True

data Point = Point Float Float deriving (Show)
data Shape = Circle Point Float | Rectangle Point Point deriving (Show)

area :: Shape -> Float
area (Circle _ r) = pi * r * r
area (Rectangle (Point x1 y1) (Point x2 y2)) = (abs $ x2 - x1) * (abs $ y2 - y1)

nudge :: Shape -> Float -> Float -> Shape
nudge (Circle (Point x y ) r) a b = Circle (Point (x+a) (y+b)) r
nudge (Rectangle (Point x1 y1) (Point x2 y2)) a b = Rectangle (Point (x1+a)(y1+b))(Point (x2+a)(y2+b))

baseCircle :: Float -> Shape
baseCircle r = Circle (Point 0 0) r

data Car = Car {  company :: String
                 ,  model :: String
                 ,  year :: Int
}deriving (Show)

tellCar :: Car -> String
tellCar (Car {company = c, model = m, year = y}) = "This " ++ c ++ " " ++ m ++ " was made in " ++ show y

-------------------------------------------------------------------------

data Vector a = Vector a a a deriving (Show)

vplus :: (Num a) => Vector a -> Vector a -> Vector a
(Vector i j k) `vplus` (Vector a b c) = Vector (i+a) (j+b) (k+c)


data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     } deriving (Eq,Show,Read)

mikeD = Person {firstName = "Michael", lastName = "Diamond", age = 43}

adRock = Person {firstName = "Adam", lastName = "Horovitz", age = 41}


mysteryDude = "Person { firstName =\"Michael\"" ++
              ", lastName =\"Diamond\"" ++
              ", age = 43}"

data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday deriving (Eq, Ord, Show, Read, Bounded, Enum)


type Obvious = String -> String

masss :: Obvious
masss k = k ++ "dfdf" 

xmoand :: Int -> Obvious
xmoand w str = str

dsdsd :: Int -> Int
dsdsd = (+0)
-------------------------------------------------------------------------
data LockerState = Taken | Free deriving (Show, Eq)

type Code = String

type LockerMap = Map.Map Int (LockerState, Code)

lockerLookup :: Int -> LockerMap -> Either String Code
lockerLookup lockerNum map = case Map.lookup lockerNum map of
  Nothing -> Left $ "Locker " ++ show lockerNum ++ " doesn\'t exist."
  Just (state , code) -> if state /= Taken
                         then Right code
                         else Left $ "Locker " ++ show lockerNum
                             ++ " is taken."

lockers :: LockerMap
lockers = Map.fromList
          [(100,(Taken, "ZD39I"))
          ,(101,(Free, "JAH3I"))
          ,(103,(Free, "IQSA9"))
          ,(105,(Free, "QOTSA"))
          ,(109,(Taken, "893JJ"))
          ,(110,(Taken, "99292"))
          ]

data List a = Empty | a :-: (List a) deriving (Show,Eq,Read,Ord)

--------------------------------------------------------------------------

data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show)

singleton :: a -> Tree a
singleton num = Node num EmptyTree EmptyTree

treeInsert :: (Ord a) => a -> Tree a -> Tree a
treeInsert x EmptyTree = singleton  x
treeInsert x (Node a left right) 
  | x == a = Node x left right
  | x <  a = Node a (treeInsert x left) right
  | x >  a = Node a left (treeInsert x right)

treeElem :: (Ord a) => a -> Tree a -> Bool
treeElem x EmptyTree = False
treeElem x (Node a left right)
  | x == a = True
  | x >  a = treeElem x left
  | x <  a = treeElem x right

data TrafficLight = Red | Yellow | Green deriving (Eq ,Show)

-- instance Eq TrafficLight where
--   Red == Red = True
--   Green == Green = True
--   Yellow == Yellow = True
--   _ == _ = False


-- instance Show TrafficLight where
--   show Red = "Red light"
--   show Yellow = "Yellow light"
--   show Green = "Green light"


ide = id
