elem' _ [] = False
elem' num (x:xs)  = if x == num then True else elem' num xs

nub [] = []
nub (x:xs)
  | elem' x xs  = nub xs
  | otherwise   = x : nub xs

isAsc []  = True
isAsc [x] = True
isAsc (x:y:xs) =
  (x <= y) && isAsc (y:xs)
