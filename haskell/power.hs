
power a n 
        | n == 0 = 1
        | even n =  (power a (div n 2)) *  (power a (div n 2))
        | otherwise = a*(power a (n-1))
