
exponent' :: Int -> Int -> Int
exponent' _ 0 = 1
exponent' b n = b * exponent' b (n-1)
