
main :: IO()
main = do
      -- putStr "Enter date: "
      -- input1 <- getLine
      input1 <- getDate
      putStr "Enter month : "
      input2 <- getLine
      putStr "Enter year  : "
      input3 <- getLine
      -- if (year `mod` 4 == 0) && (month > 2)
      --         let sum = fcentury year + fdate date + fmonth month + fyear year + 1
      --     else
      --         let sum = fcentury year + fdate date + fmonth month + fyear year 
      let  date = (read  input1 :: Int)
      let  month = (read input2 :: Int)
      let  year = (read input3 :: Int)
      putStr "Day is "
      -- input4 <- getLine
      putStrLn $ fday (fsum date month year `mod` 7)
      -- putStrLn $ fday date

-- getDate :: IO
getDate = do
  putStr "Enter date: "
  getLine

fsum date month year
  | (year `mod` 4) == 0 && month > 2 = fcentury year + fdate date + fmonth month + fyear year + 1
  | otherwise = fcentury year + fdate date + fmonth month + fyear year 

fday :: Int -> String
fday num
  |num == 0 = "Sunday"
  |num == 1 = "Monday"
  |num == 2 = "Tuesday"
  |num == 3 = "Wednesday"
  |num == 4 = "Thursday"
  |num == 5 = "Friday"
  |num == 6 = "Saturday"
  |otherwise = error "Calculation went wrong"

fdate :: (Ord a , Num a) => a -> a
fdate num = if num < 31 && num > 0  then num else error "Date is not valid."

fmonth :: Int -> Int
fmonth num
  |num == 1 = 0
  |num == 2 = 3
  |num == 3 = 3
  |num == 4 = 4
  |num == 5 = 1
  |num == 6 = 4
  |num == 7 = 6
  |num == 8 = 2
  |num == 9 = 5
  |num == 10 = 0
  |num == 11 = 3
  |num == 12 = 5
  |otherwise = error "Calculation went wrong"

fyear num =
      let solved = (num  `mod` 100) - 1
      in (solved + solved `div` 4)

fcentury num
  | solved == 1 = 5
  | solved == 2 = 3
  | solved == 3 = 1
  | solved == 0 = 0
  where solved = (num `div` 100) `mod` 4
