main = do
  putStr "Count to four for me:"
  putStrLn "one, two"
  putStrLn ", three, and"
  putStr " four!"
  print ((+) 0 blah)
  where blah = negate 1
-- test = "trivial"
-- test a = a ++ " 1"
-- test a b = a ++ b

-- convert xs = (\(x:xs) -> if xs == [] then x else x ++ )
convert (x:xs)
  | xs == [] = x
  | otherwise = x ++ convert xs

-- reverse' xs = foldr (:[]) xs 

power 0 x = 1
power n x = x * power (n-1) x

factorial n = product [1..n]

-- exponent' num = sum map (\x -> ) 

compareTriplets a b =  [length $ filter id alice , length $ filter id bob]
        where bob = zipWith  (>) b a
              alice = zipWith (>) a b
              
diagonalDifference :: Int ->   [[Int]] -> [Int]
diagonalDifference 0 (x:xs) = [head x] 
diagonalDifference n (x:xs) = x !! n : diagonalDifference (n-1) xs
          
diagonalDifference' :: Int -> Int ->   [[Int]] -> [Int]
diagonalDifference' n 0 (x:xs) = x !! n : [] 
diagonalDifference' n i (x:xs) = x !! (n-i) : diagonalDifference' n (i-1) xs

main' :: [[Int]] -> Int
main' arr =
  abs $ (sum $ diagonalDifference n arr) - (sum $ diagonalDifference' n n arr)
  where n = (length arr) - 1

plusMinus :: Fractional a => [Int] -> [a]
plusMinus arr =
  [(fromIntegral  (length  (filter (>0) arr))) / fromIntegral n, fromIntegral (length $ filter (<0) arr) / fromIntegral n, fromIntegral (length $ filter (==0) arr) / fromIntegral n]
  where n = length arr


-- printN 0 str 
printN n char
   | n <= 0  = ""
   | otherwise = char : printN (n-1) char 



stairCase :: (Ord t , Num t) => t -> t -> String 
stairCase n i
  | n >= i = (printN (n-i) ' ') ++  printN (i) '#' -- ++ "\n" 
  | otherwise = error "Overflow"

-- stairCaseN n i
--         | n == i  = stairCase n 0
--         | otherwise =  do
                       
                       
  
stairCaseN n = map (stairCase n ) [1..n]

main'' [x] = putStrLn $ x
main'' (x:xs) = do
                putStrLn $ x
                main'' xs

master = do
  print 3
  putStr " "
  print 4

hour s =
  read (Prelude.takeWhile (/= ':') s)
  
conv s = show $ 12 + s

timeConversion s
          | ((s !! 8) == 'P' && (hour s)  >= 1 && (hour s) <= 11) = conv (hour s) ++ Prelude.drop 2  (Prelude.takeWhile (/= 'P') s)   
          | (s !! 8) == 'A' &&  hour s == 12 = "00"  ++ Prelude.drop 2  (Prelude.takeWhile (/= 'A') s) 
          | (s !! 8) == 'A'  =  (Prelude.takeWhile (/= 'A') s)
          | ((s !! 8) == 'P')  =  (Prelude.takeWhile (/= 'P') s)
          
  -- if (s !! 8) == 'P' then conv (Prelude.takeWhile (/= ':') s) ++ drop 2  (Prelude.takeWhile (/= 'P') s) else (Prelude.takeWhile (/= 'A') s)
  -- where conv s = show $ 12 + read s

gradingStudent s
  | s < 38 = s
  | s `mod` 5 >= 3 = (s `div` 5 + 1) * 5
  | otherwise = s

applesAnd a arr =
  map (+a) arr

countApplesAndOranges s t a b  apples oranges = do
  print $ sum $ [1 | x <- apples , a+x >= s ]
  print $ sum $ [1 | x <- oranges , b+x <= t ]

kangaroo x1 v1 x2 v2
  | v1 <= v2 = "NO"
  | t == 0  = "YES"
  | otherwise = "NO"
  where t = mod (x1-x2) (v2-v1)



s = let x = 7
        y = negate x
        z = y*10
    in z/x+y

emacs :: Num a => a 
emacs = 3

x = (+)

f :: [a] -> Int
f xs = w `x` 1
  where w = length xs

type Name = String
type Fname = String
type Lname = String

data Pet = Cat | Dog Fname Lname  deriving Show

animal ::  Lname -> Pet
animal  = Dog "ilaya" 

ramp :: (Num a,Num b) => a -> b -> b
ramp x y = y

jackal :: (Ord a, Eq b) => a -> b -> b
jackal a b = b


idM :: a -> a -> a
idM a _ = a

idM' :: a -> a -> a
idM' _ a = a

myCom x = x > length [1..19] 

bigNum = (^) 5 $ 10

myFunc :: (x -> y) -> (y -> z) -> c -> (a,x) -> (a,z)
myFunc xToY yToZ _ (a, x) = (a , (yToZ . xToY) x) 

co :: (b -> c) -> (a -> b) -> a -> c
co f g  = f.g

-- data Identity a = Identity
newtype Identity a = Identity a

instance Eq a => Eq (Identity a) where
  (==) (Identity v) (Identity v') = v == v'

newtype TisAnInteger =  TisAn Integer

instance Eq TisAnInteger  where
  (==) (TisAn a)  (TisAn b) = a == b

data TwoIntegers =
  Two Integer Integer

instance Eq TwoIntegers where
  (==) (Two a1 a2) (Two b1 b2) = a1 == b1 && a2 == b2

data StringOrInt =
  TisAnInt Int
  | TisAString String

instance Eq StringOrInt where
  (==) (TisAnInt a) (TisAnInt b) = a == b
  (==) (TisAString a) (TisAString b) = a == b

data Pair a =
  Pair a a

instance Eq a => Eq (Pair  a) where
  (==) (Pair v1 v2) (Pair v3 v4) = (v1 == v3) && (v1 == v2) && (v1 == v4)

data Tuple a b =
  Tuple a b

instance (Eq a,Eq b) => Eq (Tuple a b) where
  (==) (Tuple v1 v2) (Tuple v3 v4) = (v1 == v3) && (v2 == v4)

data Which a =
  ThisOne a
  | ThatOne a

instance Eq a => Eq (Which a) where
  (==) (ThisOne v)  (ThisOne v') = v == v'
  (==) (ThatOne v)  (ThatOne v') = v == v'

